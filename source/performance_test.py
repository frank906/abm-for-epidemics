"""
Test for performance
"""
import time
import numpy as np
from scipy.spatial import distance

class agent:
    def __init__(self,loc):
        self.loc = loc
        
    def distance(self, agent):
        return np.sqrt((self.loc[0]-agent.loc[0])**2 + \
               (self.loc[1]-agent.loc[1])**2)

# Create N agents with a location associated
N = 10000
pop = []
for i in range(N):
    loc = np.random.random()*100, np.random.random()*100
    ag = agent(loc)
    pop.append(ag)
    
# Compute distances between all agents
ti = time.time()
dist = np.zeros([N,N])
for i in range(N):
    ag = pop[i]
    for j in range(i+1,N):
        agj = pop[j]
        dist[i,j] = ag.distance(agj)
        dist[j,i] = dist[i,j]
tf = time.time() - ti

# Alternative way of computing distances (using scipy cdist)
ti = time.time()
# Create list of points
np_points = np.array([a.loc for a in pop])
# Compute points
dists = distance.cdist(np_points,np_points)
tf2 = time.time() - ti
    

