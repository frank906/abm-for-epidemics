"""
Epidemiologic Agent-Based Model example
Following:
"""
import random as rnd
import matplotlib.pyplot as plt
from scipy.spatial import distance
import numpy as np
import random

import ogr

import time

from .plot_tools import plot_shp

class agent:
    """

    atts:
        - id [int]: Unique identification integer
        - home [loc]: Household location object
        - work [loc]: Work location object
        - age [int]: Age of the agent
        - health [str]: current health status from ['S','E','A','S0','S1','D'] corresponding to ['Susceptible', 'Exposed', 'Asymptomatic', 'Mild symptoms', 'Severe Symptoms', 'Dead']
        - inf_days [int]: Days passed since infected
        - current_loc [loc]: Location object from current location of agent
        - location_log [list]: List of location objects the agents has gone through each step
        - health_loh [list]: List of health status through each step 

    """
    def __init__(self, id, home, work, age):
        self.id = id
        self.home = home
        self.work = work
        self.age = age
        # Initialize infection progression parameters
        self.health = 'S'
        self.inf_days = 0
        # Initialize arrays
        self.current_loc = self.home
        self.location_log = [self.current_loc]
        self.health_log = [self.health]

    def distance(self, agent):
        """
        Geometric distance between two agents (coordinates from location should be in projected units!!)
        """
        return (self.current_loc.loc[0]-agent.current_loc.loc[0])**2 + \
               (self.current_loc.loc[1]-agent.current_loc.loc[1])**2

    def move_to(self, destination):
        self.current_loc = destination

    def update_log(self):
        self.location_log.append( self.current_loc )
        self.health_log.append( self.health )

    def get_infectivity(self):
        """
        Assign infectivity level of agent. 1 means has the largest capacity of infecting others.
        """
        if self.health == 'E':
            return 0
        elif self.health == 'A':
            return 0.6
        elif self.health in ['S0','S1']:
            return 1
        else:
            return 0

    def update_health(self):
        """
        Iupdate agent's current health_status based on the days since infection of the agent (self.inf_days)
        """
        if self.health in ['S','R','D']: return

        elif self.inf_days <= self.latent_duration:
            self.health == 'E'
        elif self.latent_duration < self.inf_days <= \
                self.latent_duration + self.infection_duration:
            if self.symptoms == 0: self.health = 'A'
            elif self.symptoms == 1: self.health = 'S0'
            elif self.symptoms == 2: self.health = 'S1'
        else:
            if self.is_mortal: self.health = 'D'
            else: self.health = 'R'

    def get_disease_evolution(self, disease):
        """
        Once the agent gets infected, assign the parameters associated to the disease evolution. Length of exposed period, length of infectious period, symptoms, death. All from the probability distributions from disease object
        """
        # Disease parameters
        disease_params = disease.get_parameters(self.age)
        # Latent period (Exposed)
        mu = disease_params['gamma_l'][0]
        sigma = disease_params['gamma_l'][1]
        cov = sigma/mu
        log_std = np.sqrt(np.log(1+cov**2))
        log_mu = np.log(mu) - 0.5*cov**2
        self.latent_duration = np.random.lognormal(log_mu, log_std)
        # Infectious period
        mu = disease_params['gamma_i'][0]
        sigma = disease_params['gamma_i'][1]
        cov = sigma/mu
        log_std = np.sqrt(np.log(1+cov**2))
        log_mu = np.log(mu) - 0.5*cov**2
        self.infection_duration = np.random.lognormal(log_mu, log_std)
        # Symptoms
        sev = disease_params['severe']
        mild = disease_params['mild']
        u = rnd.random()
        if u <= sev: self.symptoms = 2
        elif sev < u < sev+mild: self.symptoms = 1
        else: self.symptoms = 0
        # Mortality
        if self.symptoms == 2: # Mortality given severe
            self.is_mortal = rnd.random() < \
                            disease_params['cfr']/disease_params['severe']
        else:
            self.is_mortal = 0

    def get_infected(self, disease):
        """
        Change agent's health status from susceptible to exposed and assign disease evolution parameters
        """
        self.health = 'E'
        self.get_disease_evolution(disease)

    def check_contagion(self, disease, beta, I, dt):
        """

        The probability of contagion for an agent follows a Poisson distribution
        with rate proportional to the time spent (dt), and number of infected
        neighbors (I).

        The probability of getting infected is the probability of getting,
        at least, one contact with an infected:

        P(infected) = 1 - P(contacts I = 0) = 1 - exp(-beta*I(dt)

        args:
            - disease [Dis]: Disease object
            - beta [float]: number of potentially infectious contacts per unit time
            - I [float]: Sum of infectivities of all neighbors
            - dt [float]: time-span
        
        """
        # Mean rate of contagion
        rate = beta*I*dt
        # Probability of getting infected
        PI = 1 - np.exp(-rate)
        # Is infected
        u = rnd.random()
        if u < PI:
            self.get_infected(disease)

    def reset(self):
        # Initialize infection progression parameters
        self.health = 'S'
        self.inf_days = 0
        # Initialize arrays
        self.current_loc = self.home
        self.location_log = [self.current_loc]
        self.health_log = [self.health]

class Population:
    """

    A Population object is a collection of agents objects

    atts:
        - agents [list]: list of agent objects
        - n [int]: Total number of agents in population
        - sird_log [dict]: The list with the number of agents deaggregated per health status over all time-steps.
        - susceptible [list]: List with all susceptible agents objects at current step
        - exposed [list]: List with all exposed agents objects at current step
        - asymptomatic [list]: List with all asymptomatic agents objects at current step
        - mild [list]: List with all agents with mild symptoms objects at current step
        - severe [list]: List with all agents with severe symptoms objects at current step
        - infected [list]: List with all infected agents (exposed + asymptomatic + mild + severe) objects at current step
        - dead [list]: List with all dead agents objects at current step
        - recovered [list]: List with all recovered agents objects at current step
        - alive [list]: List with all agents alive (all - dead) objects at current step
        - sird_log [dict]: Dict with the number of agents per health status over all steps.

    """
    def __init__(self, agents):
        self.agents = agents
        self.n = len(agents)
        self.update_status()
        self.sird_log = {'S':[], 'I':[], 'E':[], 'A':[], 'S0':[], 'S1':[],
                         'R':[], 'D':[]}

    def update_status(self):
        """
        Group agents by health status
        """
        self.susceptible = [ag for ag in self.agents if ag.health=='S']
        self.exposed = [ag for ag in self.agents if ag.health == 'E']
        self.asymptomatic = [ag for ag in self.agents if ag.health == 'A']
        self.mild = [ag for ag in self.agents if ag.health == 'S0']
        self.severe = [ag for ag in self.agents if ag.health == 'S1']
        self.infected = self.exposed + self.asymptomatic + self.mild + \
                        self.severe
        self.recovered = [ag for ag in self.agents if ag.health=='R']
        self.dead = [ag for ag in self.agents if ag.health=='D']
        self.alive = self.susceptible + self.infected + self.recovered
        self.sird = {'S':len(self.susceptible), 'I':len(self.infected),
                     'E':len(self.exposed), 'A':len(self.asymptomatic),
                     'S0':len(self.mild), 'S1':len(self.severe),
                     'R':len(self.recovered), 'D':len(self.dead)}

    def get_global_neighbors(self, agent, group, radius):
        """
        For a given 'agent', get all agents from 'group' (e.g. susceptible) that are closer to 'radius' distance.
        """
               
        neighbors = [nb for nb in group if \
                            nb.distance(agent) < radius**2 and nb!=agent]
        
        # np_points = np.array([ag.current_loc.loc for ag in group if ag!=agent])
        # dists = distance.cdist(np_points, np.array([agent.current_loc.loc]))
        # neighbors = [group[i] for i in range(np.size(dists,0)) if dists[i] < \
        #              radius]
        
        # if neighbors!=neig2:
        #     self.aux=1
        
        return neighbors

    def get_local_neighbors(self, agent, group):
        """
        For a given 'agent' get all agents from 'group' (e.g. susceptible) that are in the same location object.
        """
        loc = agent.current_loc
        neighbors = [nb for nb in group if nb.current_loc == loc]
        return neighbors

    def update_log(self):
        """
        Update the health and locations log list from each agent, and the list of agents per health status from the entire population
        """
        # Update location log
        for ag in self.agents:
            ag.update_log()
        # Update aggregated variables log
        for status, number in self.sird.items():
            self.sird_log[status].append( number )

    def reset(self):
        for ag in self.agents:
            ag.reset()

class Location:
    """
    
    A location is a geographical place that hosts many agents. It can be a household, a work/school place or a recreational place.

    atts:
        - id [int]: Location unique identification number
        - loc [tuple]: Geographic coordinates of centroid of location
        - area [float]: area value of location (UNUSED!!)
        - census_block_id [int]: Id number of the census block where the location belongs to.
        - occup_type [str]: Type of location: household, work/school, recreational
        - n_floor [int]: Number of floors (UNUSED!!)
        - agents [list]: List of all agents objects that inside location
        - n_agents [int]: number of agents inside location
        - n_infected [int]: number of infected agents inside location
    
    """
    def __init__(self, id, centroid, area, census_block_id, occup_type,
                 n_floors):
        self.id = id
        self.loc = centroid
        self.area = area
        self.cbid = census_block_id
        self.occup_type = occup_type
        self.n_floors = n_floors
        self.agents = []
        self.n_agents = 0
        self.n_infected = 0

    def add_agent(self, agent):
        self.agents.append( agent )
        self.n_agents += 1

class CensusBlock:
    """
    
    Census block is an administrative unit where the user has population / locations data to create the model. 
    
    It is ONLY USED to assign properties to the locations and agents from the information of the census block (e.g. in this case, the population total).

    atts:
        - id [int]: Unique identification integer
        - population [int]: Number of people in the census block
        - n_locations [int]: number of location that compose the census block
        - locations [dict]: dictionary of locations (by id)
    
    """

    def __init__(self, id, population):
        self.id = id
        self.population = population
        self.n_locations = 0
        self.locations = {}

    def add_location(self, location):
        self.locations[location.id] = location
        self.n_locations += 1

    def get_locations(self, type):
        """
        
        Get all location of given type from this census block
        
        """
        return [i for i in self.locations.values() if i.occup_type==type]

class City:

    """
    
    A city is formed by a Population object and a collection of location objects.
    
    atts:
        - population [Pop]: A population object
        - locations [dict]: Dictionary of locations (by id)
        - beta [dict]: Dictionary with rate of infectious contacts by agent by unit time, and by locatio type
        - global_radius [float]: Radius that define what are considered global neighbors for any agent.
        - types [set]: All different location types in the city
        - locations_dict [dict]: Dictionary of locations by type
        - locations_count [dict]: Dictionary of location counts by type
    
    """
    
    def __init__(self, beta, global_radius, locations={}, population=None):
        self.beta = beta
        self.global_radius = global_radius
        self.locations = locations
        self.population = population

    def classify_locations(self):
        # Extract all different types
        self.types = set([a.occup_type for a in self.locations.values()])
        # Split according to type
        self.locations_dict = {}
        self.locations_count = {}
        for type in self.types:
            self.locations_dict[type] = [a for a in self.locations.values() \
                                                    if a.occup_type==type]
            self.locations_count[type] = len(self.locations_dict[type])

    def populate_from_shp(self, census_blocks_shp, parcels_shp):
        """
        
        Method to create the Population object and the locations list from two shapefiles:
        - census_blocks_shp: Shapefile with the census blocks data
        - parcels_shp: Shapefile with all parcels (locations)
        
        """
        # Initialize output list
        census_blocks = {}
        locations = {}

        # Open shps
        source_cb = ogr.Open(census_blocks_shp)
        source_pr = ogr.Open(parcels_shp)

        # Layers
        layer_cb = source_cb.GetLayer()
        layer_pr = source_pr.GetLayer()

        # Create census blocks objects
        for feature in layer_cb:
            id = feature.GetField('RADIO_I')
            population = int(feature.GetField('POBLACI'))
            cb = CensusBlock(id, population)
            census_blocks[id] = cb
        layer_cb.ResetReading()
        source_cb = None

        # Create locations objects
        for feature in layer_pr:
            id = feature.GetField('smp')
            geom = feature.GetGeometryRef()
            centroid = geom.Centroid().GetX(), geom.Centroid().GetY()
            area = feature.GetField('area')
            censusblock_id = feature.GetField('RADIO_I')
            occup_type = feature.GetField('TIPO1_16')
            n_floors = feature.GetField('PISOS_16')
            loc = Location(id, centroid, area, censusblock_id, occup_type,
                           n_floors)
            # Add to dictionary
            locations[id] = loc
            # Add to correspondent census block
            census_blocks[censusblock_id].add_location( loc )
        layer_pr.ResetReading()
        source_pr = None

        # Save locations
        self.locations = locations
        self.classify_locations()

        # Populate according to data from census blocks
        agents = []
        households = self.locations_dict['E']
        workplaces = self.locations_dict['L']
        schools = self.locations_dict['EDU']
        for cb in census_blocks.values():
            # Get all households from locations
            hh = [h for h in households if h.cbid==cb.id]
            # Create agents
            for i in range(cb.population):
                id = i
                # Assign to some household at random
                home = random.choice(hh)
                age = np.random.uniform(low=5, high=80)
                if age <= 25:
                    work = random.choice(schools)
                elif 25 < age <= 70:
                    work = random.choice(workplaces)
                elif age > 70:
                    work = home
                ag = agent(id, home, work, age)
                home.add_agent( ag )
                agents.append( ag )

        # Create population
        self.population = Population(agents)

##
class Disease:
    """
    Class for Disease properties and parameters.
    
    RIGHT NOW ONLY WORKS AS A DATA STRUCTURE OF DISTRIBUTION PARAMETERS
    """
    def __init__(self, gamma_l=0, gamma_i=5, mild=0.2, severe=0.05,
                 cfr=0.01):
        self.parameters = {'gamma_l':gamma_l, 'gamma_i':gamma_i,
                           'mild':mild, 'severe':severe, 'cfr': cfr}

    def get_parameters(self, age):
        """
        """
        return self.parameters

class Epidemic:
    """
    
    Formed by a city object and a disease object. It has the methods to move step by step and peform actions to the population.
    
    atts:
        - city [City]: City object
        - disease [Disease]: Disease object
    
    """
    
    def __init__(self, city, disease):
        self.city = city
        self.disease = disease

    def initiate_infectious(self, n0_infected):
        """
        Infects n0_infected agents from the population
        """
        # Infect initial agents
        for i in range(n0_infected):
            self.city.population.agents[i].get_infected(self.disease)
        # Initiate population status and logs
        self.city.population.update_status()
        self.city.population.update_log()

    def action(self, action):
        """
        Move agents around.
        
        args:
            - action [str]: Describes the type of action that agents will do. Where they are gonna move

        """
        population = self.city.population
        if action == 'work': # Move everyone to work/school location
            for ag in population.alive:
                ag.move_to( ag.work )

        elif action == 'home': # Move everyone to home location
            for ag in population.alive:
                ag.move_to( ag.home )

        elif action == 'rec': # Move everyone to recreation locations
            ss = len(population.alive)
            aux = random.choices(self.city.locations_dict['LOTE'], k=ss)
            i = 0
            for ag in population.alive:
                ag.move_to( aux[i] )
                i += 1

            # for ag in population.alive:
            #     # random rec location
            #     rec = random.choice(self.city.locations_dict['LOTE'])
            #     ag.move_to( rec )

    def update(self, step, dts):
        """
        It runs a simulation step (1 day). Here, each step is formed by three actions: 'agents at work', 'agents at home', 'agents at rec'.
        
        During each action, it checks agents infect each other.
        
        At the end of the step, it updates the health-status of all infected agents (+1 on days of infection).

        """
        # 3 actions a day
        actions = ['work','home','rec']
        for i in range(len(actions)):
            ti = time.time()
            # Move agents
            self.action(actions[i])
            # Check population new infections
            beta_local = self.city.beta[actions[i]]
            beta_global = self.city.beta['global']
            self.propagate_disease(beta_local, beta_global, dts[i])
            print('Disease propagation at {} - {}s'.format(actions[i],
                                                           time.time()-ti))
        # Check infected population evolution
        ti = time.time()
        self.disease_evolution(step)
        print('Within-host evolution {}s'.format(time.time()-ti))
    
    def propagate_disease(self, beta_local, beta_global, dt):
        """
        Checks how agents infect each other
        
        THIS METHOD COULD BE INSIDE POPULATION CLASS!!
        """
        # ti = time.time()
        # # Check for new infections in population
        # for ag in self.city.population.susceptible:
        #     # Check local neighbors (in same location)
        #     local_neighbors = self.city.population.get_local_neighbors(ag,
        #                                     self.city.population.infected)
        #     if len(local_neighbors) > 0:
        #         infectivities = [n.get_infectivity() for n in local_neighbors]
        #         ag.check_contagion(self.disease, beta_local,
        #                             sum(infectivities), dt)
        #     if ag.health == 'S': # If it was not infected in previous step

        #         # Check number of infected global neighbors
        #         global_neighbors = self.city.population.get_global_neighbors(ag,
        #                                     self.city.population.infected,
        #                                     self.city.global_radius)
        #         global_neighbors = [x for x in global_neighbors if x \
        #                                                 not in local_neighbors]
        #         if len(global_neighbors) > 0:
        #             infectivities = [n.get_infectivity() for n in \
        #                                                     global_neighbors]
        #             ag.check_contagion(self.disease, beta_global,
        #                             sum(infectivities), dt)
        # tf = time.time() - ti
        
        # ti = time.time()
        # Check for global infections
        s_points = np.array([ag.current_loc.loc for ag in \
                                    self.city.population.susceptible])
        i_points = np.array([ag.current_loc.loc for ag in \
                                    self.city.population.infected])
        dists = distance.cdist(s_points, i_points)
        row, col = np.where(dists < self.city.global_radius)
        ags = 0 * dists
        # Number of global neighbors per infected
        global_mask = (dists < self.city.global_radius) & (dists!=0)
        n_global_neighbors = np.sum(global_mask,0)
        # Identify global neighbors
        ags[global_mask] = beta_global
        # Correct global beta values per number of neighbors
        ags = ags * (1/n_global_neighbors)
        # Identify local neighbors
        ags[dists == 0] = beta_local
        # Infected infectivities
        infs = np.array([n.get_infectivity() for n in \
                                    self.city.population.infected])
        # Sum infectivities of neighbors for every susceptible
        infectivities = np.sum( ags * infs ,1)
        # Ratio
        rat = 1 - np.exp( -infectivities * dt )
        u = np.random.rand(len(rat))
        contagiated = u < rat
        for i in np.where(contagiated==True)[0]:
            self.city.population.susceptible[i].get_infected(self.disease)
        # tf2 = time.time() - ti
        
        # Update population health status
        self.city.population.update_status()

    def disease_evolution(self, step):
        """
        It updates the health status of all agents in population
        
        THIS METHOD COULD BE INSIDE POPULATION CLASS!!
        """
        # Check evolution for infected agents
        for ag in self.city.population.infected:
            ag.inf_days += step
            ag.update_health()
        # Update population health status
        self.city.population.update_status()
        # Update population agents log
        self.city.population.update_log()

    def run_simulation(self, n0_infected, step, n_steps, dts):
        """
        Run simulation.
        
            - n0_infected [int]: number of initially infected agents
            - step [float]: Number of days per step. USE 1!
            - n_steps [int]: Number of steps to perform. If 'auto' it will perform steps until there are no more infected agents
        
        """
        self.step = step
        self.n_steps = n_steps
        # Initiate infection
        self.initiate_infectious(n0_infected)
        # Fixed number of steps
        if n_steps == 'auto':
            counter = 0
            while self.city.population.sird['I'] > 0:
                self.update(step, dts)
                counter += 1
            n_steps = counter
        else:
            for i in range(n_steps):
                print('Running step {} of {} ----------------'.format(i+1,n_steps))
                self.update(step, dts)
    
    def city_snapshot(self, shapefile, i, title=''):
        """
        
        Updates the output shapefile with the number of infected per location, at step 'i'. THE OUTPUT SHAPEFILE HAS TO EXIST. SHOULD BE THE SAME AS PARCELS SHAPEFILE, OR THE SHAPEFILE USED TO CREATE THE LCOATIONS.
        
        Plots the shapefile using matplotlib
        
        """
        # Edit fields in output shapefile
        ds = ogr.Open(shapefile, 1)
        lyr = ds.GetLayer()
        
        # Loop through infected at step i
        infected = [ag for ag in self.city.population.agents if \
                            ag.health_log[i] in ['E', 'A', 'S0', 'S1']]
        for agent in infected:
            loc_id = agent.home.id
            self.city.locations[loc_id].n_infected += 1
            
        # Look for feature with same id
        for feature in lyr:
            feat_id = feature.GetField('id')
            try:
                ninf = self.city.locations[feat_id].n_infected
            except:
                pass
            feature.SetField('ninfected', ninf)
            lyr.SetFeature(feature)
        lyr.ResetReading()
        ds = None
        
        # Plot shapefile
        plot_shp(shapefile, 'ninfected', num='All', units='', colors='Paired', 
                 size=(9,6), ret=1)

    def plot_sird(self, status=['D','I','S','R'], colors=['r','y','b','g'],
                  i=None, ax=None):
        """
        Plot disease evolution curves until step 'i'. It plots only the health status define in 'status' with the colors defined in 'colors'.
        """
        
        if not i:
            i = self.n_steps
        N = len(self.city.population.agents)
        # arrays to plot
        plot_arrays = []
        for stat in status:
            plot_arrays.append( \
                        np.array(self.city.population.sird_log[stat])[:i+1] )

        # Format x-axis
        if not ax:
            fig, ax = plt.subplots()
        ax.set_xlabel('days')
        ax.xaxis.set_ticks(np.arange(0,self.n_steps+1,int(1/self.step)))
        ax.xaxis.set_ticklabels(np.arange(0,self.n_steps+1,int(1/self.step)))
        ax.tick_params(axis='x', rotation=45)
        ax.set_xlim([0,len(plot_arrays[0])-1])
        # ax.set_ylim([0,1])

        # Plot in order
        y_top = 0*plot_arrays[0]
        counter = 0
        for array in plot_arrays:
            y_bottom = y_top
            y_top = y_top + array
            ax.fill_between(x=range(len(array)), y1=y_bottom, y2=y_top,
                            facecolor=colors[counter])
            counter += 1

        ax.legend(status)
        # Correct y-limits
        y_lim_inf = max(ax.get_ylim()[0],0)
        # y_lim_sup = min(ax.get_ylim()[1],1)
        y_lim_sup = ax.get_ylim()[1]
        ax.set_ylim([y_lim_inf, y_lim_sup])

    def agents_snapshot(self, i, ax=None, title=''):
        """
        
        Plot all agents (as circles) at their location, with a color reflecting its health status.
        
        NOT USED!! WAS USEFUL FOR BOX MODEL, BUT NOT FOR CITY MODEL, SINCE MANY AGENTS WILL BE IN SAME LOCATION.
        
        """
        population = self.population
        susceptible = [ag for ag in population.agents if ag.health_log[i]=='S']
        infected = [ag for ag in population.agents if \
                                    ag.health_log[i] in ['E', 'A', 'S0', 'S1']]
        recovered = [ag for ag in population.agents if ag.health_log[i]=='R']
        dead = [ag for ag in population.agents if ag.health_log[i]=='D']
        # Plot agents at their current location
        if not ax:
            fig, ax = plt.subplots()
        ax.plot([ag.location_log[i][0] for ag in susceptible],
                [ag.location_log[i][1] for ag in susceptible],
                "bo", markersize=0.2)
        ax.plot([ag.location_log[i][0] for ag in infected],
                [ag.location_log[i][1] for ag in infected],
                 "yo", markersize=0.2)
        ax.plot([ag.location_log[i][0] for ag in recovered],
                [ag.location_log[i][1] for ag in recovered],
                 "go", markersize=0.2)
        ax.plot([ag.home_loc[0] for ag in dead],
                [ag.home_loc[1] for ag in dead],
                 "ro", markersize=0.2)
        # Plot infection radius for infected
        for ag in infected:
            circle = plt.Circle((ag.location_log[i][0], ag.location_log[i][1]),
                                self.infectious_ratio, color='y', fill=False)
            ax.add_patch(circle)
        # Add title text
        text = ax.text(0.85, 0.95, "", transform=ax.transAxes, fontsize=6)
        text.set_text(title)
        # Format axes
        ax.axis("image")
        ax.axis([0,1,0,1])
    
    def create_agents_plots(self, ncols, sn):
        """
        NOT USED!
        """
        # Precalculate number of plots
        nplots = int(self.n_steps/sn)+1
        nrows = int(int(nplots/ncols + 0.99))
        fig, axes = plt.subplots(nrows,ncols)
        if nrows == 1:
            axes = np.array([axes])
        return axes
    
    def agents_snapshot_history(self, observations=(3,1)):
        """
        NOT USED!
        Plot in same figure agents snapshots for each time step.
        """
        counter = 0
        ncols = observations[0]
        sn = observations[1]
        axes = self.create_agents_plots(ncols, sn)
        for i in range(self.n_steps):
            # Plot observation
            if observations:
                if i % sn == 0:
                    nrow = int(counter/ncols)
                    ncol = counter % 3
                    ax = axes[nrow,ncol]
                    title = 'step ' + str(i) + ' - ' + self.last_action
                    self.agents_snapshot(i, ax, title)
                    counter += 1

## SIMULATION
if __name__ == "__main__":
    """
    City example
    """
    import time
    
    n_agents = 1000
    n0_infected = 1
    global_ratio = 300
    step = 1
    n_steps = 15
    beta = {'home':0.0007, 'work':0.0003, 'rec':0.0002, 'global':0.000001}

    # Create city
    census_blocks_shp = 'Databases\\census_blocks_utm.shp'
    parcels_shp = 'Databases\\parcels_utm.shp'
    city = City(beta, global_ratio)
    city.populate_from_shp(census_blocks_shp, parcels_shp)

    # Create disease
    disease_params = {'gamma_l':[2,0.5], 'gamma_i':[7,3], 'mild':0.2,
                      'severe':0.04, 'cfr':0.01}
    disease = Disease(**disease_params)

    # Simulation
    ti = time.time()
    epidemic = Epidemic(city, disease)
    epidemic.run_simulation(n0_infected, step, n_steps)
    tf = time.time() - ti
    epidemic.plot_sird(status=['D','S1','S0','A','E'],
                       colors=['r','w','m','y','y','b','g'])
    epidemic.city_snapshot('source\\databases\\run_output.shp', n_steps, 'step '+str(n_steps))
    # epidemic.agents_snapshot_history((3,5))
    plt.show()
