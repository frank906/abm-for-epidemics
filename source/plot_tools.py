## Imports
import sys
import os
import csv
import math

import gdal
import ogr
import osr
import numpy as np
import matplotlib
from matplotlib.collections import PatchCollection
import matplotlib.path as mpath
import matplotlib.patches as mpatches
import matplotlib.pyplot as plt
import matplotlib.colors as colors

import source.interactive_plot as interactive_plot

class MidpointNormalize(colors.Normalize):
    """Normalize colormap with a different ranges for pos and neg values
    """
    def __init__(self, vmin=None, vmax=None, midpoint=None, clip=False):
        self.midpoint = midpoint
        colors.Normalize.__init__(self, vmin, vmax, clip)

    def __call__(self, value, clip=None):
        # I'm ignoring masked values and all kinds of edge cases to make a
        # simple example...
        x, y = [self.vmin, self.midpoint, self.vmax], [0, 0.5, 1]
        return np.ma.masked_array(np.interp(value, x, y))

def plot_shp(shapefile, field_plot, num='All', units='', colors='RdBu_r', 
             size=(9,6), ret=0):
    """ Plot a .shp file for a given field. The features with field value equal
    to 0, will be displayed in a different color than the rest.
    
    Keyword arguments:
        fig (obj): matplotlib.figure object
        shapefile (str): full path name of shapefile
        field_plot (str/np array): 
            string: name of field to plot from current shp
            np array/list: numbers to plot onto current shp
        num (int/str): number of feature to plot; 'All' to plat all features
                       (default, 'All')
        units (str): unit name to print in colorbar title (default, '')
        colors (str): cmap color type (default, 'Reds')
            http://matplotlib.org/examples/color/colormaps_reference.html
            
    
    Raises:
        IndexError: field_plot np array should be same number than features
                    of shapefile
    """
    fig = plt.figure(figsize=size)
    
    # Extract first layer of features from shapefile using OGR
    ds = ogr.Open(shapefile)
    nlay = ds.GetLayerCount()
    lyr = ds.GetLayer()
    
    # get all fields names
    lyrdfn = lyr.GetLayerDefn()
    field_names = []
    for n in range(lyrdfn.GetFieldCount()):
        fdefn = lyrdfn.GetFieldDefn(n)
        field_names.append(fdefn.name)
    
    # Get extent and calculate buffer size
    ext = lyr.GetExtent()
    xoff = (ext[1]-ext[0])/50
    yoff = (ext[3]-ext[2])/50
    
    # set offsets to have same aspect ratio than figure
    fig_width = fig.get_size_inches()[0]*80
    fig_height = fig.get_size_inches()[1]*80
    rho_obj = fig_height/fig_width
    rho = (ext[3]-ext[2]+2*yoff)/(ext[1]-ext[0]+2*xoff)
    if rho >= rho_obj:
        xoff = 1/(2*rho_obj)*(ext[3]-ext[2]+2*yoff)-(ext[1]-ext[0])/2
    else:
        yoff = rho_obj/2*(ext[1]-ext[0]+2*xoff)-(ext[3]-ext[2])/2
        
    # Prepare figure
    ax = fig.add_subplot(111, autoscale_on=True)
    ax.set_xticks([])
    # ax.set_xticklabels([])
    ax.set_yticks([])
    # ax.set_yticklabels([])
    ax.set_xlim(ext[0]-xoff,ext[1]+xoff)
    ax.set_ylim(ext[2]-yoff,ext[3]+yoff)
    
    paths = []
    paths2 = []
    lyr.ResetReading()
    
    # Read all features in layer and store as paths
    if num == 'All' or num == 'ALL' or num == 'all':
        num = range(len(lyr))
    field = []
    field2 = []
    field_dict = {}
    for k in num:
        feat = lyr.GetFeature(k)
        Geom = feat.geometry()
        
        # get fields from shapefile
        field_value = {}
        for fname in field_names:
            field_index = feat.GetFieldIndex(fname)
            field_value[fname] = feat.GetField(fname)
        
        # create polygons
        for m in range(0,Geom.GetGeometryCount()):
            geom = Geom.GetGeometryRef(m)
            codes = []
            all_x = []
            all_y = []
            geom_num = geom.GetGeometryCount()
            if geom_num >0:
                for i in range(max(1,geom_num)):
                    # Read ring geometry and create path
                    r = geom.GetGeometryRef(i)
                    x = [r.GetX(j) for j in range(r.GetPointCount())]
                    y = [r.GetY(j) for j in range(r.GetPointCount())]
                    # skip boundary between individual rings
                    codes += [mpath.Path.MOVETO] + \
                                (len(x)-1)*[mpath.Path.LINETO]
                    all_x += x
                    all_y += y
            elif geom_num == 0:
                x = [geom.GetX(j) for j in range(geom.GetPointCount())]
                y = [geom.GetY(j) for j in range(geom.GetPointCount())]
                # skip boundary between individual rings
                codes += [mpath.Path.MOVETO] + \
                            (len(x)-1)*[mpath.Path.LINETO]
                all_x += x
                all_y += y
                path = mpatches.Polygon(np.column_stack((all_x,all_y)),
                        codes, picker=True)
                paths.append(path)
                field.append(field_value[field_plot])
    
    # Set patch collection and colors
    p = PatchCollection(paths, alpha=0.8)
    p.set_edgecolor('k')
    p.set_linewidth(0.2)
    p.set_picker(True)
    p.set(array = np.array(field))
    
    n_integers = int(max(p.get_array()))+1
    c_map = []
    for i in range(n_integers):
        a = i/n_integers
        c_map.append([1,1-a,1-a])
    cm = matplotlib.colors.ListedColormap(c_map)
    p.set_cmap(cm)
    # Add a colour bar
    divs = n_integers
    divs_list = [i for i in range(divs)]
    divs_ticks = [i+0.5 for i in range(divs)]
    divs_list_names = [str(i) for i in divs_list]
    cbar = fig.colorbar(p, ticks=divs_list)
    cbar.ax.set_yticklabels(divs_list_names) # vertically oriented colorbar
    # cbar.ax.set_ylabel(field_plot+' '+ units)
    
    # Change size
    matplotlib.rcParams.update({'font.size':6})
    
    # show plot
    ax.grid(False)
    # ax.set_picker(False)
    ax.set_aspect('equal')
    ax.set_facecolor((0.7, 0.7, 0.7))
    ax.add_collection(p)
    ax.collections[0].set_zorder(0)
    
    # set interactive zoom-panning
    scale = 1.2
    zp = interactive_plot.ZoomPan()
    figZoom = zp.zoom_factory(ax, base_scale = scale)
    figPan = zp.pan_factory(ax)
    figPick = zp.pick_factory(ax)
    
    if ret:
        pass
    else:
        plt.show()
    
    return ax.get_figure()