import time
import matplotlib.pyplot as plt

from source.EpidemicABM import City, Disease, Epidemic

## Fixed run for testing
# import random
# import numpy as np
# random.seed(30)
# np.random.seed(40)

## INPUT PARAMETERS =======================================================
n0_infected = 1 # Initially infected agents
global_ratio = 300 # Distance in meters for global neighbors
 # Time of the day spend at ['work/school', 'home', 'rec']
dts = [8,14,2]
# Rate of potentially infectious contacts per agent per unit time
beta = {'home':0.001, 'work':0.0005, 'rec':0.0002, 'global':0.00003}
# Distribution parameters of disease
disease_params = {'gamma_l':[0.0001,0.5], 'gamma_i':[7,3], 'mild':0.2,
                  'severe':0.04, 'cfr':0.01}
# Shapefiles used to build city
census_blocks_shp = 'source\\databases\\census_blocks_utm.shp'
parcels_shp = 'source\\databases\\parcels_utm.shp'

## CREATE MODEL ===========================================================
# Create city
city = City(beta, global_ratio)
city.populate_from_shp(census_blocks_shp, parcels_shp)

# Create disease
disease = Disease(**disease_params)

## CREATE SIMULATION =======================================================
step = 1 # each step is 1 day
n_steps = 50 # Run 60 days

ti = time.time()
epidemic = Epidemic(city, disease)
epidemic.run_simulation(n0_infected, step, n_steps, dts)
tf = time.time() - ti

# PLOT RESULTS ============================================================
epidemic.plot_sird(status=['D','S1','S0','A','E'],
                    colors=['r','w','m','y','y','b','g'])
output_shapefile = 'source\\databases\\run_output.shp'
epidemic.city_snapshot(output_shapefile, n_steps, 'step '+str(n_steps))
plt.show()